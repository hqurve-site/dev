
# Layout
- [./content](./content/) contains the content as submodules
    - `food`
    - `fun`
    - `main`: this contains the sites main page
    - `notes`: this contains all notes.
        To add new notes, just add a folder under components with the necessary structure. (See other examples)
- [./fonts](./fonts) contains fonts for use.
- [./latex-to-antora](./latex-to-antora) contains the code used to convert latex documents to notes in antora.
- [./playbook](./playbook/) contains the antora instructions to build the site.
    It also contains the (node) dependencies used for development.
- [./tools](./tools)
    - `compress-images` looks for folders called `_raw-images` (recursively) and compresses images (reduces dimensions)
    according to the `config.json` file.
    The original files are left unchanged. This is done for large images (eg photographs).

        Example config file
        ```json
        {
            "file1.jpg": { "dimensions": 2000, "size": 1000 },
            // ...
        }
        ```
        All unmatched files are assumed to have `{"dimensions": 600, "size": 50}`.
        The following commands are done for each file
        ```bash
        convert $filename -resize "$($dimensions)x" $outfile # uses imagemagick
        jpegoptim --size=$size $outfile # uses https://github.com/tjko/jpegoptim
        ```
        Only jpg files are supported. The outfile has the same name as the original file
        but is in the same directory as `_raw-images`.


    - `gen-images` looks for all svg files and converts them to png using inkscape.
        This is done to ensure that svg images are displayed correctly.
    
- [./ui](.ui) contains the ui template for the website. It is a fork of https://gitlab.com/antora/antora-ui-default.

Currently, there is a bit or regret of using submodules instead of a single large repo.
In the future, I may combine all content into a single repo.

# Development

## Simple
If you have the necessary tools available (mostly just nodejs), you can build the site using
- `npx antora local-playbook.yml`. This builds the website for quick viewing
- `npx antora publish-playbook.yml`. This builds the website for release

Please ensure to be in the playbook directory

## Podman/docker
The `podman-compose` may be used to isolate the build from your system. Use
- `podman-compose up live-server` to rebuild the site whenever changes are made.
    If accessed by `localhost:8008`, the page autoreloads.
- `podman-compose up interactive` to load a development environment. (Currently, only has node).

If you change the Containerfile, be sure to run `podman-compose build`.

