#!/usr/bin/env node

// written for nodejs 14
// requires imagemagick and jpegoptim

const { stat, readdir, readFile } = require('fs/promises');
const util = require('util');
const { exec } = require('child_process');

const RAW_DIR = "_raw-images";
const CONFIG_FILE = "config.json";
const MAX_AGE = 120; // time in seconds

async function execAsync (command) {
  return new Promise((resolve, reject) => {
    // https://stackoverflow.com/a/15257991
    const child = exec(command, (error, stdout, stderr) =>{
      if (error){
        reject(error)
      }else{
        resolve();
      }
    });
    child.stdout.pipe(process.stdout);
    child.stderr.pipe(process.stderr);
  });
}
async function findRawDirs(path) {
  let stats = await stat(path);

  if (stats.isDirectory()) {
    if (path.endsWith(`/${RAW_DIR}`)) {
      return [path];
    }else{
      let contents = await readdir(path);
      let subpaths = contents.map(c => `${path}/${c}`).map(findRawDirs);
      return Promise.all(subpaths).then(arr => arr.flat());
    }
  }else{
    return [];
  }
}

async function getOutputPath(path) {
  return path.substring(0, path.length - RAW_DIR.length - 1);
}
async function getFiles(path) {
  let children = await readdir(path);
  return children.filter(x => x != CONFIG_FILE);
}
async function getConfigFile(dirpath) {
  try{
    let path = `${dirpath}/${CONFIG_FILE}`;
    let stats = await stat(path);
    if (! stats.isFile()) return {};

    let contents = await readFile(path);

    return JSON.parse(contents);
  }catch(err) {
    return {};
  }
}
async function getConfig(configFile, force) {
  configFile.default = configFile.default || {};

  for (let key in configFile ){
    let obj = configFile[key];
    obj.dimensions = obj.dimensions || "600";
    obj.size = obj.size || "50";
  }
  let self = {
    getAttribute: function(filename) {
      return configFile[filename] || configFile.default;
    },
    getWidth: function(filename) {
      return + self.getAttribute(filename).dimensions;
    },
    getSize: function(filename) {
      return + self.getAttribute(filename).size;
    },
    skipFile: async function(filename, path, outpath) {
      if (force) return false;
      let stats = await stat(`${path}/${filename}`);

      try {
        await stat(`${outpath}/${filename}`); //err or is thrown if file does not exist
      }catch (err){
        return false;
      }

      return Date.now() - stats.mtimeMs > MAX_AGE * 1000;
    },
  };
  return self;
}

async function convertFile(path, outpath, filename, config) {
  if (await config.skipFile(filename, path, outpath)) return;
  let infile = `${path}/${filename}`;
  let outfile = `${outpath}/${filename}`;

  if (filename.endsWith(".jpg")) {
    console.log(`${infile} -> ${outfile}`);
    await execAsync(`convert ${infile} -resize ${config.getWidth(filename)}x ${outfile}`);
    await execAsync(`jpegoptim --size=${config.getSize(filename)} ${outfile}`);
  }else{
    console.log(`unknown filetype: ${path}/${filename}`);
  }
}

async function handleRawDir(path, force) {
  let outputDir = await getOutputPath(path);
  let configFile = await getConfigFile(path);
  let config  = await getConfig(configFile, force);

  let files = await getFiles(path);

  for (const name of files) {
    await convertFile(path, outputDir, name, config);
  }


}


async function main(force) {
  for (const path of await findRawDirs("content")) {
    await handleRawDir(path, force);
  }
}

try {
  let force = process.argv.some(e => e == "--force");
  main(force);
}catch (error){
  console.err(error);
}

