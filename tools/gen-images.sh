#!/usr/bin/env bash

export time=$(date +%s)
export maxage=120

myconvert () {
    file=$1
    outfile="${file%%.svg}.png"
    lastedit=$(stat -c "%Z" "$file")

    if [[ $(($lastedit + $maxage)) -ge $time || ! -f "$outfile" ]]; then
        echo "$file -> $outfile"
        inkscape "$file" --export-dpi=240 -C -o "$outfile"
    fi
}

export -f myconvert
find -wholename '*/images/*.svg' -exec bash -c 'myconvert "{}"' \;
