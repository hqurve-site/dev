#!/usr/bin/env bash

exec_loc=$(dirname "$(realpath "$0")")

inpath=$(realpath "$1")
outpath=$(realpath "$2")
texpath="$outpath/old-tex"

if [[ -d "$outpath" ]]; then
    # echo "outpath exist"
    # exit
    rm -r "$outpath"
fi
mkdir -p "$outpath"


cp -a "$(dirname "$inpath")" "$texpath"

#pushd "$outpath" > /dev/null
#git init

unwanted="aux log xdv synctex.gz fls fdb_latexmk toc"

for ext in $unwanted
do
    find "$outpath" -name "*.$ext" -exec rm {} \;
done
if [[ -d "$texpath/svg-inkscape" ]]; then
    rm -r "$texpath/svg-inkscape"
fi


pushd "$outpath" > /dev/null

git init
git add old-tex
git commit -m "init"

"$exec_loc/convert.sh" old-tex/"$(basename "$1")" .


